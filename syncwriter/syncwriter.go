package syncwriter

import (
	"io"
	"sync"
)

// SyncWriteCloser implements io.WriteCloser with sync.Mutex protection.
type SyncWriteCloser struct {
	wc io.WriteCloser

	mtx sync.Mutex
}

// NewSyncWriteCloser wraps an existing io.WriteCloser.
func NewSyncWriteCloser(w io.WriteCloser) *SyncWriteCloser {
	return &SyncWriteCloser{
		wc: w,
	}
}

// Write writes given buffer in a thread-safe mode.
func (w *SyncWriteCloser) Write(p []byte) (int, error) {
	w.mtx.Lock()
	defer w.mtx.Unlock()

	return w.wc.Write(p)
}

// Close closes the Writer.
func (w *SyncWriteCloser) Close() error {
	w.mtx.Lock()
	defer w.mtx.Unlock()

	return w.wc.Close()
}
